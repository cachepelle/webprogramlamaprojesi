﻿using HamzaProje.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace HamzaProje.DAL
{
    public class icerikContext : DbContext
    {
        public icerikContext() : base("DefaultConnection")
        {

        }
        public DbSet<Haberler> Haber { get; set; }
        public DbSet<Duyurular> Duyuru { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}