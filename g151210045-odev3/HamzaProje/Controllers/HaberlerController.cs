﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HamzaProje.DAL;
using HamzaProje.Models;

namespace HamzaProje.Controllers
{
    public class HaberlerController : Controller
    {
        private icerikContext db = new icerikContext();

        // GET: Haberler
        public ActionResult Index()
        {
            return View(db.Haber.ToList());
        }

        // GET: Haberler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haber.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // GET: Haberler/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Haberler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HaberlerID,Baslik,Tarih,icerik,Resim")] Haberler haberler)
        {
            if (ModelState.IsValid)
            {
                db.Haber.Add(haberler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(haberler);
        }

        // GET: Haberler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haber.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // POST: Haberler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HaberlerID,Baslik,Tarih,icerik,Resim")] Haberler haberler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(haberler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(haberler);
        }

        // GET: Haberler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haber.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // POST: Haberler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Haberler haberler = db.Haber.Find(id);
            db.Haber.Remove(haberler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
