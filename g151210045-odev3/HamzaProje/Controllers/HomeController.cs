﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HamzaProje.DAL;
using HamzaProje.Models;
using PagedList;

namespace HamzaProje.Controllers
{
    public class HomeController : Controller
    {
        private icerikContext db = new icerikContext();
        public ActionResult Index(int? page)
        {
            var duyurular = from s in db.Duyuru
                            select s;

            int pageSize = 3;
            int pageNumber = (page ?? 1);


            return View(duyurular.OrderBy( x  => x.Tarih ).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}