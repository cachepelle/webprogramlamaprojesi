﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HamzaProje.Models
{
    public class Duyurular
    {
        public int DuyurularID { get; set; }
        public string Baslik { get; set; }
        public DateTime Tarih { get; set; }
        public string icerik { get; set; }
        public byte[] Resim { get; set; }

    }
}