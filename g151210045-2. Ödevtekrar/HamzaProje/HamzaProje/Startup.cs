﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HamzaProje.Startup))]
namespace HamzaProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
